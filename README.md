# Straightway

Straightway is a network storage and communication framework. It is planned to have various
applications, ranging from messaging and social media to equal and secret ballot. These
applications shall use the network storage layer
[peerspace](https://codeberg.org/straightway/peerspace) as a basis.

Currently, [peerspace](https://codeberg.org/straightway/peerspace) is under development, the
applications mentioned above are in a very early conceptual phase.

# HELP WANTED!

This project is currently a one-man-show, and this one man also has a regular job, a family,
friends and other activities. So if you would like to contribute, just send me an
[e-mail](mailto:straightway@zoho.eu). I need help with:

* Development: Write new features and applications, fix bugs, clean up and complete the code. 
* Organization: Set up a website, try to find more contributors, organize teams, talk to the press,
  etc.
  
Let me make clear: These are NO JOBS, all is voluntary and non-paid.
 
# Sub-projects:

* [peerspace](https://codeberg.org/straightway/peerspace): A peer to peer distributed network storage
  layer.
* [sim](https://codeberg.org/straightway/sim): Event driven simulation library.
* [sim.net](https://codeberg.org/straightway/sim/tree/master/net): Event driven network simulation
  library.
* [testing](https://codeberg.org/straightway/testing): Ease unit testing with JUnit 5 in kotlin.
* [numbers](https://codeberg.org/straightway/numbers): Math calculations with mixed number types
* [units](https://codeberg.org/straightway/units): Attaching units to numbers and converting them.
* [random](https://codeberg.org/straightway/random): Random numbers.
* [expr](https://codeberg.org/straightway/expr): Partially bound functional expressions.
* [koinutils](https://codeberg.org/straightway/koinutils): Utlitities for the Koin dependency
  injection framework.
* [utils](https://codeberg.org/straightway/utils): General utility classes and functions.
* [error](https://codeberg.org/straightway/error): Error utility library.

All projects have in common:

* [Apache-2.0 License](https://codeberg.org/straightway/straightway/src/branch/master/buildTemplates/LICENSE)
* [Common coding style](https://codeberg.org/straightway/straightway/src/branch/master/buildTemplates/Coding.md)
* Kotlin programming language
* Gradle build system
* The libraries can be accessed as binaries via the maven repository <https://pages.codeberg.org/straightway/repo>.
* Contributing: End an email to [straightway@zoho.eu](mailto:straightway@zoho.eu).